import 'package:flutter/material.dart';
import 'package:gecko/providers/substrate_sdk.dart';
import 'package:gecko/providers/wallet_options.dart';
import 'package:gecko/widgets/balance_display.dart';
import 'package:provider/provider.dart';

class Balance extends StatelessWidget {
  const Balance({super.key, required this.address, required this.size, this.color = Colors.black});
  final String address;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final walletOptions = Provider.of<WalletOptionsProvider>(context, listen: false);
    return Consumer<SubstrateSdk>(builder: (context, sdk, _) {
      return FutureBuilder(
          future: sdk.getBalance(address),
          builder: (BuildContext context, AsyncSnapshot<Map<String, int>> globalBalance) {
            if (globalBalance.connectionState != ConnectionState.done || globalBalance.hasError || !globalBalance.hasData) {
              if (walletOptions.balanceCache[address] != null && walletOptions.balanceCache[address] != -1) {
                return BalanceDisplay(value: walletOptions.balanceCache[address]!, size: size, color: color);
              } else {
                return const SizedBox.shrink();
              }
            }
            walletOptions.balanceCache[address] = globalBalance.data!['transferableBalance']!;
            if (walletOptions.balanceCache[address] != -1) {
              return BalanceDisplay(value: walletOptions.balanceCache[address]!, size: size, color: color);
            } else {
              return const Text('');
            }
          });
    });
  }
}
