// ignore_for_file: file_names

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko/globals.dart';
import 'package:gecko/screens/onBoarding/3.dart';
import 'package:gecko/widgets/commons/intro_info.dart';
import 'package:gecko/widgets/commons/offline_info.dart';
import 'package:gecko/widgets/commons/top_appbar.dart';

class OnboardingStepTwo extends StatelessWidget {
  const OnboardingStepTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: GeckoAppBar('yourMnemonic'.tr()),
      body: SafeArea(
        child: Stack(children: [
          SingleChildScrollView(
            child: InfoIntro(
                text: 'keepThisMnemonicSecure'.tr(),
                assetName: 'fabrication-de-portefeuille-impossible-sans-phrase.png',
                buttonText: '>',
                nextScreen: const OnboardingStepThree(),
                pagePosition: 1),
          ),
          const OfflineInfo(),
        ]),
      ),
    );
  }
}
